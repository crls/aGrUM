
/**
 *
 *  Copyright 2005-2019 Pierre-Henri WUILLEMIN et Christophe GONZALES (LIP6)
 *   {prenom.nom}_at_lip6.fr
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <iostream>
#include <sstream>
#include <string>

#include <cxxtest/AgrumTestSuite.h>
#include <cxxtest/testsuite_utils.h>

#include <agrum/BN/algorithms/BayesBall.h>
#include <agrum/BN/generator/simpleBayesNetGenerator.h>

namespace gum_tests {

  class BayesBallTestSuite: public CxxTest::TestSuite {
    public:
    void setUp() {}

    void tearDown() {}


    void testRequisiteNodes() {
      gum::SimpleBayesNetGenerator< double > gen(50, 200, 2);
      gum::BayesNet< double >                bn;
      gen.generateBN(bn);
      gum::Set< gum::NodeId > requisite;

      gum::Set< gum::NodeId >      query, hardEvidence, softEvidence;
      gum::Sequence< gum::NodeId > nodes_seq;
      for (const auto node: bn.nodes())
        nodes_seq.insert(node);

      for (gum::Idx i = 0; i < 5; ++i)
        hardEvidence.insert(nodes_seq.atPos(i));

      for (gum::Idx j = 24; j > 19; --j)
        query.insert(nodes_seq.atPos(j));

      TS_ASSERT_THROWS_NOTHING(gum::BayesBall::requisiteNodes(
         bn.dag(), query, hardEvidence, softEvidence, requisite));

      TS_ASSERT(requisite.size() >= 5);
    }
  };
}   // namespace gum_tests
