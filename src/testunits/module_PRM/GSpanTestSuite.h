
/**
 *
 *  Copyright 2005-2019 Pierre-Henri WUILLEMIN et Christophe GONZALES (LIP6)
 *   {prenom.nom}_at_lip6.fr
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <cxxtest/AgrumTestSuite.h>
#include <cxxtest/testsuite_utils.h>

#include <agrum/PRM/inference/gspan.h>

#include <agrum/PRM/o3prm/O3prmReader.h>

namespace gum_tests {

  class GSpanTestSuite: public CxxTest::TestSuite {
    private:
    gum::prm::o3prm::O3prmReader< double >*    __driver;
    std::string                                dot_dir;
    gum::prm::gspan::InterfaceGraph< double >* ig;

    void local__setUp() {
      __driver = new gum::prm::o3prm::O3prmReader< double >();
      __driver->readFile(GET_RESSOURCES_PATH("o3prm/specialprinters.o3prm"));

      if (__driver->errors() == 0) {
        ig = new gum::prm::gspan::InterfaceGraph< double >(
           __driver->prm()->getSystem("m"));
      } else {
        __driver->showElegantErrorsAndWarnings();
        TS_ASSERT(false);
      }
    }

    void local__tearDown() {
      delete ig;

      if (__driver->prm() != nullptr) { delete __driver->prm(); }

      delete __driver;
    }

    public:
    void testInit() {
      gum::prm::o3prm::O3prmReader< double > driver;
      driver.readFile(GET_RESSOURCES_PATH("o3prm/specialprinters.o3prm"));
      TS_ASSERT(driver.prm() != nullptr);

      if (driver.prm() != nullptr) delete driver.prm();
    }

    void testInterfaceGraph() {
      try {
        local__setUp();
        TS_ASSERT_EQUALS(ig->graph().size(),
                         (gum::Size)1 + 5 * 2 + 4 * 3 + 4 * 3 + 5 + 3 + 4);
        TS_ASSERT_EQUALS(ig->graph().sizeEdges(),
                         (gum::Size)(5 * 2 + 3 * 4 + 4 * 3) + 5 + 3 * 3 + 4 * 2);
        local__tearDown();
      } catch (gum::Exception& e) { GUM_SHOWERROR(e); }
    }

    void testTree() {
      local__setUp();
      gum::prm::GSpan< double >* gspan = nullptr;
      TS_GUM_ASSERT_THROWS_NOTHING(
         gspan = new gum::prm::GSpan< double >(*(__driver->prm()),
                                               __driver->prm()->getSystem("m")));
      TS_ASSERT(gspan != nullptr);

      if (gspan != nullptr) {
        TS_GUM_ASSERT_THROWS_NOTHING(gspan->discoverPatterns());
        TS_GUM_ASSERT_THROWS_NOTHING(delete gspan);
      }

      local__tearDown();
    }
  };

}   // namespace gum_tests
