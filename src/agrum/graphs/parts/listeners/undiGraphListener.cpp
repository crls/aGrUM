
/**
 *
 *  Copyright 2005-2019 Pierre-Henri WUILLEMIN et Christophe GONZALES (LIP6)
 *   {prenom.nom}_at_lip6.fr
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


/** @file
 * @brief source file for virtual Base classes for non-oriented graphs listener
 *
 * @author Pierre-Henri WUILLEMIN
 */

#include <agrum/graphs/parts/listeners/undiGraphListener.h>

#ifdef GUM_NO_INLINE
#  include <agrum/graphs/parts/listeners/undiGraphListener_inl.h>
#endif   // GUM_NOINLINE

namespace gum {

  UndiGraphListener::UndiGraphListener(const UndiGraphListener& d) {
    GUM_CONS_CPY(UndiGraphListener);
    GUM_ERROR(OperationNotAllowed, "No copy constructor for UndiGraphListener");
  }

  UndiGraphListener& UndiGraphListener::operator=(const UndiGraphListener& d) {
    GUM_OP_CPY(UndiGraphListener);
    GUM_ERROR(OperationNotAllowed, "No copy operator for UndiGraphListener");
  }

  UndiGraphListener::UndiGraphListener(UndiGraph* g) {
    if (!g) {
      GUM_ERROR(OperationNotAllowed, "A graph listener need a graph to listen to");
    }

    GUM_CONSTRUCTOR(UndiGraphListener);
    _graph = g;

    GUM_CONNECT((*_graph), onNodeAdded, (*this), UndiGraphListener::whenNodeAdded);
    GUM_CONNECT(
       (*_graph), onNodeDeleted, (*this), UndiGraphListener::whenNodeDeleted);
    GUM_CONNECT((*_graph), onEdgeAdded, (*this), UndiGraphListener::whenEdgeAdded);
    GUM_CONNECT(
       (*_graph), onEdgeDeleted, (*this), UndiGraphListener::whenEdgeDeleted);
  }

  UndiGraphListener::~UndiGraphListener() { GUM_DESTRUCTOR(UndiGraphListener); }

}   // namespace gum
