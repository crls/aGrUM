
/**
 *
 *  Copyright 2005-2019 Pierre-Henri WUILLEMIN et Christophe GONZALES (LIP6)
 *   {prenom.nom}_at_lip6.fr
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


/**
 * @file
 * @brief Classes of signaler.
 *
 * For more documentation, @see signaler0.h
 *
 * @author Pierre-Henri WUILLEMIN and Christophe GONZALES
 *
 */
#ifndef GUM_SIGNALER_H
#define GUM_SIGNALER_H
#include <functional>

#include <agrum/core/signal/listener.h>

#include <agrum/core/signal/signaler0.h>
#include <agrum/core/signal/signaler1.h>
#include <agrum/core/signal/signaler2.h>
#include <agrum/core/signal/signaler3.h>
#include <agrum/core/signal/signaler4.h>
#include <agrum/core/signal/signaler5.h>
#include <agrum/core/signal/signaler6.h>
#include <agrum/core/signal/signaler7.h>

#endif   // GUM_SIGNALER_H
