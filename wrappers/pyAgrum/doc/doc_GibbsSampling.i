%feature("docstring") gum::GibbsSampling
"
Class for making Gibbs sampling inference in bayesian networks.

GibbsSampling(bn) -> GibbsSampling
    Parameters:
      * **bn** (*pyAgrum.BayesNet*) -- a Bayesian network
"