Exceptions from aGrUM
=====================

All the classes inherit GumException's functions errorType, errorCallStack and errorContent.

.. autoexception:: pyAgrum.DefaultInLabel
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.DuplicateElement
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.DuplicateLabel
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.EmptyBSTree
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.EmptySet
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.GumException
	:exclude-members: with_traceback
.. autoexception:: pyAgrum.FatalError
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.FormatNotFound
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.GraphError
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.IOError
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.IdError
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.InvalidArc
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.InvalidArgument
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.InvalidArgumentsNumber
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.InvalidDirectedCycle
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.InvalidEdge
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.InvalidNode
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.NoChild
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.NoNeighbour
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.NoParent
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.NotFound
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.NullElement
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.OperationNotAllowed
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.OutOfBounds
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.OutOfLowerBound
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.OutOfUpperBound
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.ReferenceError
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.SizeError
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.SyntaxError
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.UndefinedElement
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.UndefinedIteratorKey
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.UndefinedIteratorValue
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
.. autoexception:: pyAgrum.UnknownLabelInDatabase
	:exclude-members: errorCallStack, errorContent, errorType, with_traceback
