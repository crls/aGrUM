Fragment of Bayesian networks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This class proposes a shallow copy of a part of Bayesian Network. It can be used as a Bayesian Network for inference algorithms (for instance).

.. autoclass:: pyAgrum.BayesNetFragment