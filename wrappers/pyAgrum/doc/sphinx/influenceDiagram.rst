Influence Diagram
=================

Model
-----
.. autoclass:: pyAgrum.InfluenceDiagram
			:members:
			:exclude-members: setProperty, property, propertyWithDefault


Inference
---------
.. autoclass:: pyAgrum.InfluenceDiagramInference
			:members:
