Credal Networks
===============

Model
-----

.. autoclass:: pyAgrum.CredalNet

Inference
---------

.. autoclass:: pyAgrum.CNMonteCarloSampling

.. autoclass:: pyAgrum.CNLoopyPropagation
			:exclude-members: asIApproximationSchemeConfiguration
