Probabilistic Relational Models
===============================

For now, pyAgrum only allows to explore Probabilistic Relational Models written with o3prm syntax.

.. autoclass:: pyAgrum.PRMexplorer
  :members:
