Module notebook
===============

.. automodule:: pyAgrum.lib.notebook

.. image:: _static/pyAgrum_all.png

Helpers
-------

.. autofunction:: pyAgrum.lib.notebook.configuration
.. autofunction:: pyAgrum.lib.notebook.sideBySide


Visualization of Potentials
---------------------------

.. autofunction:: pyAgrum.lib.notebook.showProba
.. autofunction:: pyAgrum.lib.notebook.getPosterior
.. autofunction:: pyAgrum.lib.notebook.showPosterior
.. autofunction:: pyAgrum.lib.notebook.getPotential
.. autofunction:: pyAgrum.lib.notebook.showPotential

Visualization of graphs
-----------------------

.. autofunction:: pyAgrum.lib.notebook.getDot
.. autofunction:: pyAgrum.lib.notebook.showDot

.. autofunction:: pyAgrum.lib.notebook.getGraph
.. autofunction:: pyAgrum.lib.notebook.showGraph

Visualization of graphical models
---------------------------------
.. autofunction:: pyAgrum.lib.notebook.getBN
.. autofunction:: pyAgrum.lib.notebook.showBN
.. autofunction:: pyAgrum.lib.notebook.getInference
.. autofunction:: pyAgrum.lib.notebook.showInference
.. autofunction:: pyAgrum.lib.notebook.getJunctionTree
.. autofunction:: pyAgrum.lib.notebook.showJunctionTree
.. autofunction:: pyAgrum.lib.notebook.showInformation
.. autofunction:: pyAgrum.lib.notebook.getInformation
.. autofunction:: pyAgrum.lib.notebook.showInfluenceDiagram
.. autofunction:: pyAgrum.lib.notebook.getInfluenceDiagram

Visualization of approximation algorithm
----------------------------------------

.. autofunction:: pyAgrum.lib.notebook.animApproximationScheme
