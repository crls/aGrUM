Model
-----

.. autoclass:: pyAgrum.BayesNet
			:exclude-members: setProperty, property, propertyWithDefault
