#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ***************************************************************************
# *   Copyright (C) 2015 by Pierre-Henri WUILLEMIN                          *
# *   {prenom.nom}_at_lip6.fr                                               *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
# ***************************************************************************
from __future__ import print_function
import time

from acttools import *
from acttools.utils import notif

def main():
  #
  # options management
  initParams()
  # persistent values of current are used as default for options
  current=getCurrent()
  configureOptions(current)

  (options, args)=parseCommandLine(current)

  msvcForced=False
  if options.mvsc or options.mvsc32 or options.mvsc17 or options.mvsc17_32:
    options.static_lib=True
    options.no_fun=True
    msvcForced=True

  mingwForced=False
  if options.mingw64:
    options.no_fun=True
    mingwForced=True
    
  pyAgrumForced=False
  if "pyAgrum" in args:
    options.static_lib=True
    pyAgrumForced=True

  # colors, fun & verbosity partly controlled by options (--no-fun, ...)
  configureOutputs(options)

  about()

  if msvcForced:
    notif("Options [static] and [no-fun] forced by option [mvsc], [mvsc32] or [mvsc17] or [mvsc17_32]")
    print()

  if mingwForced:
    notif("Options [no-fun] forced by option [mingw64]")
    print()

  if pyAgrumForced:
    notif("Options [static] forced by target [pyAgrum]")
    print()

  configureTools(options)

  # check current consistency and update it if necessary
  checkCurrent(current,options,args)

  #
  # from now, current contains the specification

  # looking at special commands (clean/show/etc.)
  gc=gm=gb=0
  if not specialActions(current):
    # creating "classical" compilation from cmake/make/postprocess
    safe_cd(current,"build")
    safe_cd(current,current["mode"])

    for target in current["targets"]:
      t0 = time.time()
      if options.build=="all":
        buildCmake(current,target)
      t1 = time.time()
      if options.build!="no-make":
        buildMake(current,target)
      t2 = time.time()
      buildPost(current,target)
      t3 = time.time()
      gc+=t1-t0
      gm+=t2-t1
      gb+=t3-t2

    safe_cd(current,"..")
    safe_cd(current,"..")

  return (gc,gm,gb)

import traceback
import sys
if __name__ == "__main__":
    try:
      tc,tm,tp=main()
      if tc+tm+tp!=0:
        notif("Time spent in cmake : [{0:0.3f}]s , make : [{1:0.3f}]s and post : [{2:0.3f}]s ".format(tc,tm,tp))
    except KeyboardInterrupt:
      print("\n Shutdown requested...exiting \n")
      sys.exit(1)
    except Exception:
      traceback.print_exc(file=sys.stdout)
      sys.exit(1)
